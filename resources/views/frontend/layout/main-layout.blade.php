<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>{{ Config::get('constants.APP_NAME') }}@yield('title')</title>

    <!-- Responsive Window -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Custom fonts -->
    {!! Html::style('https://fonts.googleapis.com/css?family=Open+Sans:400,300,700') !!}
    {!! Html::style('fonts/foundation-icons/foundation-icons.css') !!}

    <!-- CSS Files -->
    {!! Html::style('assets/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('css/frontend/main.css') !!}
    @yield('css')
  </head>

  <body>
    @include('frontend.include.header')

    <div id="ui-main">
      @yield('content')
    </div>

    @include('frontend.include.footer')

    {!! Html::script('assets/global/scripts/jquery.min.js') !!}
    {!! Html::script('assets/bootstrap/js/bootstrap.min.js') !!}
    @yield('script')
  </body>
</html>
