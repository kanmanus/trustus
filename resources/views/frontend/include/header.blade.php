<header>
  <div id="top-bar">
    <div class="container text-right">
      <div id="more-nav">
        <a href="#" class="menu-item">LOG IN</a> | <a href="#" class="menu-item">REGISTER</a>
      </div>

      <div id="lang-bar">
        <a href="#" class="lang-item">EN</a> | <a href="#" class="lang-item">TH</a>
      </div>
    </div>
  </div>

  <div id="main-header">
    <nav class="container">
      <a href="#" class="menu-item">BOOK A TRIP</a>
      <a href="#" class="menu-item">PLAN A TRIP</a>
      <a href="#" class="menu-item" id="logo">TRUST</a>
      <a href="#" class="menu-item">TRAVEL PLANNER</a>
      <a href="#" class="menu-item">BLOG</a>
    </nav>
  </div>
</header>
