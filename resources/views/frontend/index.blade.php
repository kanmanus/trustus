@extends('frontend.layout.main-layout')

@section('title', ' - Home')

@section('css')
  {!! Html::style('css/frontend/home.css') !!}
@endsection

@section('content')
  <div id="banner-top">
    <div class="cover"></div>

    <div id="promo-text" class="text-center">
      <div class="main-text">GET INSPIRED</div>
      <div class="sub-text">We'll create the perfect trip for you in four easy steps</div>
    </div>
  </div>

  <div id="personal-trip-planner">
    <div id="instruction" class="text-center">Choose your trip preferences below</div>

    <div id="trip-type" class="block">
      <div class="container">
        <div class="headline strong">Trip Type</div>

        <div class="row">
          <div class="col-md-6">
            <div class="box">
              <div class="question">What's the purpose of your trip?</div>
              <div class="choices">
                <div class="choice">Anniversary</div>
                <div class="choice">Honeymoon</div>
                <div class="choice">Family Get Together</div>
                <div class="choice">Romantic Gateway</div>
                <div class="choice">Other</div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="box">
              <div class="question">What's your preferred trip?</div>
              <div class="choices">
                <div class="choice">Private Tour</div>
                <div class="choice">Small Group Experience</div>
                <div class="choice">Coach Tour</div>
                <div class="choice">Luxury Cruise</div>
                <div class="choice">I'm not sure</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="trip-preferences" class="block">
      <div class="container">
        <div class="headline strong">Trip Preferences</div>

        <div class="row">
          <div class="col-md-6">
            <div class="box">
              <div class="question">Where would you like to go?</div>
              <div class="choices">
                <div class="choice">Africa</div>
                <div class="choice">Asia/Pacific</div>
                <div class="choice">Europe</div>
                <div class="choice">North America</div>
                <div class="choice">South America</div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="box">
              <div class="question">What would you like to do?</div>
              <div class="choices">
                <div class="choice">Culture</div>
                <div class="choice">Safari</div>
                <div class="choice">Walking</div>
                <div class="choice">Active Adventure</div>
                <div class="choice">Other</div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="box">
              <div class="question">When would you like to go?</div>
              <div class="choices">
                <div class="choice">Departure Date</div>
                <div class="choice">Duration</div>
                <div class="choice">Departure Location</div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="box">
              <div class="question">How many travelers are going?</div>
              <div class="choices">
                <div class="choice">Number of Adults</div>
                <div class="choice">Age Group</div>
                <div class="choice">Number of Children</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="planning-information" class="block">
      <div class="container">
        <div class="headline strong">Planning Information</div>

        <div class="row">
          <div class="col-md-6">
            <div class="box">
              <div class="question">How much planning have you done?</div>
              <div class="choices">
                <div class="choice">Haven't Started Yet</div>
                <div class="choice">Putting Together Initial Ideas</div>
                <div class="choice">Actively Reviewing Trip Options</div>
                <div class="choice">Ready to Book</div>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <div class="box">
              <div class="question">What accommodations do you prefer?</div>
              <div class="choices">
                <div class="choice">Luxury (5 Stars Only)</div>
                <div class="choice">Deluxe (4 Stars & Above)</div>
                <div class="choice">First Class (3.5 Stars)</div>
                <div class="choice">Price Range</div>
              </div>
            </div>
          </div>
        </div>

        <div id="additional-comment">
          <div class="question">Do you have any special requests?</div>
          <div class="answer">
            <textarea name="special_request" rows="10" placeholder="Add Your Comment Here"></textarea>
          </div>
        </div>
      </div>
    </div>

    <div id="contact-information" class="block">
      <div class="container">
        <div class="headline strong">Contact Information</div>

        <form action="">
          <div class="form-group">
            <input type="text" name="firstname" placeholder="First Name *">
          </div>

          <div class="form-group">
            <input type="text" name="lastname" placeholder="Last Name *">
          </div>

          <div class="form-group">
            <input type="text" name="email" placeholder="Email Address *">
          </div>

          <div class="form-group">
            <input type="text" name="country_of_residence" placeholder="Country of Residence *">
          </div>

          <div class="form-group">
            <input type="text" name="phone_number" placeholder="Phone Number *">
          </div>

          <div class="form-group">
            <input type="text" name="call_time" placeholder="When Would You Like Us To Call You? *">
          </div>

          <div class="form-group">
            <button type="submit">Plan My Perfect Trip</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('script')
<script>
  $(function(){
    $(window).scroll(function() {
        if ($(document).scrollTop() > 100) {
            $('#main-header').addClass('small');
        }
        else {
            $('#main-header').removeClass('small');
        }
    });
  });
</script>
@endsection
