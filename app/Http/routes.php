<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
  'as' => 'home',
  function () {
    return view('frontend.index');
  }
]);

/* --------------------- Admin ------------------- */
Route::group(['prefix' => '_admin'], function () {
    Route::controller('login', 'Backend\LoginController');
    Route::get('logout', 'Backend\LoginController@logout');
});
Route::group(['middleware'=>'admin','prefix' => '_admin'], function () {
    Route::get('/', function () { return view('backend.index'); });
    Route::resource('user-management','Backend\AdminController');
    Route::resource('role','Backend\AdminRoleController');
    Route::resource('page','Backend\AdminPageController');
    Route::post('check-username','Backend\CheckUsernameController@checkuser');
});

/* --------------------- Theme ------------------- */
Route::group(['prefix' => '_theme'], function () {
    Route::get('blank',function(){
        return view('backend.theme_component.blank');
    });
    Route::get('form',function(){
        return view('backend.theme_component.form');
    });
    Route::get('list',function(){
        return view('backend.theme_component.list');
    });
});
